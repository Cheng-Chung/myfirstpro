import re

def play_game():
    # Check if the input is a valid email address
    user_email = input("Please enter your email address: ")

    # Prompt the user for an email address
    if is_valid_email(user_email):
        print("The email address is valid.")
        return True
    else:
        print("The email address is not valid.")
        return True

def is_valid_email(email):
    # Define the regex pattern for a valid email address
    email_pattern = r'^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$'
    
    # Use the re.match() function to check if the email matches the pattern
    if re.match(email_pattern, email):
        return True
    else:
        return False

def is_caught():

    argument = input("Please enter C and m: ")

    cat_and_mouse = list(argument)

    string_to_check = ['C','m']
# Check if any character in the string is in the list
    if all(char in cat_and_mouse for char in string_to_check):
        #cat_mouse = 'live'
        c = cat_and_mouse.index('C')
        m = cat_and_mouse.index('m')
        if c < m and m - c <= 10:
            print("cat can catch the mouse!!")
            return True
        else: 
            print("Mouse is running away")
            return True
    else:
        print(f"Not all characters from '{string_to_check}' are in the list.")
        return True    

def display_main():
    print("\nMenu:")
    print("1. valid email address")
    print("2. is caught")

def main():

    while True:
        # Placeholder for the game logic
        display_main()
        
        choice = input("Please choose an option (1-2): ").strip()

        if choice == '1':
            play_game()
        elif choice == '2':
            is_caught()
            #break
        else:
            print("Invalid input. Please enter a number between 1 and 2.")

        while True:
            # Ask the user if they want to play again
            play_again = input("Do you want to play again? (yes/no): ").strip().lower()
            
            # Check the user's response
            if play_again in ('yes', 'y'):
                
                break
            elif play_again in ('no', 'n'):
                print("Thank you for playing!")
                return
            else:
                print("Invalid input. Please enter 'yes' or 'no'.")
                
if __name__ == "__main__":
    main()
