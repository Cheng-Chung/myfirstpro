def nt_dictionary(n):
    import random
    assert n > 10
    assert n < 100
    output = {}
    for i in range(n):
        while True:
            rn = random.randint(1, 1000)
            if rn in output.keys():
                continue
            break
        output[rn] = rn * rn
    return output


print(nt_dictionary(20))
print('Length', len(nt_dictionary(20)))
