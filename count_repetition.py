def count_repetition(argument):
    output = {}
    for element in argument:
        if element in output:
            output[element] += 1
        else:
            output[element] = 1
    return output


print(count_repetition(['kerouac', 'fante', 'fante', 'buk', 'hemingway', 'hornby', 'kerouac', 'buk', 'fante']))
# {'kerouac': 2, 'fante': 3, 'buk': 2, 'hemingway': 1, 'hornby': 1}
