def are_same_type(my_list):
    iseq = iter(my_list)
    first_type = type(next(iseq))
    return True if all((type(x) is first_type) for x in iseq) else False


print(are_same_type(['hello', 'world', 'long sentence']))  # True
print(are_same_type([1, 2, 9, 10]))  # True
print(are_same_type([['hello'], 'hello', ['bye']]))  # False
print(are_same_type([['hello'], [1, 2, 3], [{'a': 2}]]))  # True
print(are_same_type([['hello'], set('hello')]))  # False
