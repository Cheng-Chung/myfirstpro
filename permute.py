def permute(nums):
    result_perms = [[]]
    for n in nums:
        new_perms = []
        for perm in result_perms:
            for i in range(len(perm) + 1):
                new_perms.append(perm[:i] + [n] + perm[i:])
                result_perms = new_perms
    return result_perms


print(permute([1, 2, 3]))


# Using itertools
def permute_cheat(nums):
    import itertools
    return [list(t) for t in itertools.permutations(nums)]


print(permute_cheat([1, 2, 3]))
