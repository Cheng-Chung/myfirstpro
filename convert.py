def convert(number):
    numbers_list = [int(n) for n in str(number)]
    return sorted(numbers_list, reverse=True)


print(convert(429563))  # [9, 6, 5, 4, 3, 2]
print(convert(324))  # [4, 3, 2]
