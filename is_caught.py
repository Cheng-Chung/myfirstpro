# V2: how to find 2 mouses
def is_caught(argument, amount):
    cat_and_mouse = list(argument)
    c = cat_and_mouse.index('C')
    m = cat_and_mouse.index('m')
    z = cat_and_mouse.index('z')
    amount = 0
    if (m - c <= 5) and (z - c <= 10):
        amount = 2
        return (True, amount)
    elif (m - c <= 5) and (z - c >= 10):
        amount = 1
        return ('Only catch ' + str(amount) + ' mouse', amount)
    else :
        return (False, amount)

print(is_caught('C...m...z', ''))  # True, 2
print(is_caught('C...m...........z', ''))  # False, 1
print(is_caught('C...z.....m', ''))  # False, 0
       
'''
# original code
def is_caught(argument):
    cat_and_mouse = list(argument)
    c = cat_and_mouse.index('C')
    m = cat_and_mouse.index('m')
    return True if m - c <= 3 else False


print(is_caught('C.....m'))  # False
print(is_caught('C..m'))  # True
print(is_caught('..C..m'))  # True
print(is_caught('...C...m'))  # False
print(is_caught('C.m'))  # True
'''