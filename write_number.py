def write_number(num):
    digits = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten']
    tens = ['', '', 'twenty', 'thirty', 'fourty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']

    lower_20 = digits + ['eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen',
                         'nineteen']

    if num == 0:
        # handle 0 differently
        return 'zero'
    elif num >= 0 and num < 20:
        # handle 1..19
        return lower_20[num]
    elif num < 100:
        # compose the rest
        upper = tens[num // 10]  # get upper digit
        lower = digits[num % 10]  # get lower digit (remainder of 10)

        if lower:
            return upper + "-" + lower
        else:
            return upper

    else:
        return "Cannot convert, number must be between 0 and 99"


print(write_number(1))
print(write_number(5))
print(write_number(11))
print(write_number(19))
print(write_number(20))
print(write_number(21))

print(write_number(33))
print(write_number(40))
print(write_number(99))
