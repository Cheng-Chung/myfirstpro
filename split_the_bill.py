# get user input to select which group you want to split
# def user_input():
while True:
    try:
        hand_input = input('Please enter a number: ')
        hand_input = ord(hand_input)
        if hand_input == 49:
            # return print(f'You selected: {hand_input}')
            hand_input == 1
        elif hand_input == 50:
            hand_input == 1
        break    
    except ValueError:
        print('Please enter 1 or 2')

def split_the_bill(obj):
    output = {}
    average = sum(obj.values()) / len(obj)
    for key, value in obj.items():
        output[key] = round(average - value)
    return output
# a = user_input()

if hand_input == 49:
     group = {
    'Amy': 20,
    'Bill': 15,
    'Chris': 10
}
     print(split_the_bill(group))
elif hand_input == 50:
    group = {
    'Jenny': 30,
    'Bill': 10,
    'Tim': 12
} 
    print(split_the_bill(group))
else:
    print('Nothing to report')


# original vision 
'''
def split_the_bill(obj):
    output = {}
    average = sum(obj.values()) / len(obj)
    for key, value in obj.items():
        output[key] = round(average - value)
    return output

group = {
    'Amy': 20,
    'Bill': 15,
    'Chris': 10
}
print("\n")
print(split_the_bill(group))  # { 'Amy': -5, 'Bill': 0, 'Chris': 5 }
'''