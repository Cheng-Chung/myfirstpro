# first approach
def exp(num, exponent):
    if exponent == 0:
        return 1
    result = 1
    for i in range(exponent):
        result *= num
    return result


print(exp(5, 3))  # 125
print(exp(2, 4))  # 16
print(exp(5, 1))  # 5
print(exp(6, 0))  # 1


# second approach, using recursive
def exp_recursive(num, exponent):
    if exponent == 0:
        return 1
    else:
        return num * exp_recursive(num, exponent - 1)


print(exp_recursive(5, 3))  # 125
print(exp_recursive(2, 4))  # 16
print(exp_recursive(5, 1))  # 5
print(exp_recursive(6, 0))  # 1
