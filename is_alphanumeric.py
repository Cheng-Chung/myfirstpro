from string_checker import is_string


def is_alphanumeric(text):
    return is_string(text) and bool(len([n for n in text if n.isdigit()]) == len(text))


if __name__ == '__main__':
    print("\n")
    print(is_alphanumeric('11'))  # True
    print(is_alphanumeric(['hello']))  # False
    print(is_alphanumeric('this is a long sentence'))  # False
    print(is_alphanumeric({'a': 2}))  # False
    print(is_alphanumeric("this is string....!!!"))  # False
    print(is_alphanumeric("this is string with num83r2....!!!"))  # False
