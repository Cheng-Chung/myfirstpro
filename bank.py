def bank():
    net_amount = 0
    while True:
        user_input = input("Waiting for bank inputs and output:\n->")
        if not user_input:
            break
        values = user_input.split(" ")
        operation = values[0].lower()
        amount = int(values[1])
        if operation == "d":
            net_amount += amount
        elif operation == "w":
            net_amount -= amount
    return net_amount


print(bank())
