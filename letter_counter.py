def letter_counter(data):
    d = {"UPPER CASE": 0, "LOWER CASE": 0}
    for c in data:
        if c.isupper():
            d["UPPER CASE"] += 1
        elif c.islower():
            d["LOWER CASE"] += 1
    return

def main():

    while True:
        # Placeholder for the game logic
        user_input = input("Write Your Sentence to Count:\n->")
        print(letter_counter(data=user_input))

        while True:
            # Ask the user if they want to play again
            play_again = input("Do you want to play again? (yes/no): ").strip().lower()
            
            # Check the user's response
            if play_again in ('yes', 'y'):
                
                break
            elif play_again in ('no', 'n'):
                print("Thank you for playing!")
                return
            else:
                print("Invalid input. Please enter 'yes' or 'no'.")
                
if __name__ == "__main__":
    main()

