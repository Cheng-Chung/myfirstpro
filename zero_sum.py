def zero_sum(arguments):
    output = []
    temp = []
    for i in arguments:
        for j in arguments:
            if i + j == 0:
                first_number = arguments.index(i)
                second_number = arguments.index(j)
                if first_number not in temp and second_number not in temp:
                    temp.append(first_number)
                    temp.append(second_number)
                    output.append([first_number, second_number])
    return output


print("\n")
print(zero_sum([1, 5, 0, -5, 3, -1]))  # [[0, 5], [1, 3], [2, 2]]
print(zero_sum([1, -1]))  # [[0, 1]]
print(zero_sum([0, 4, 3, 5]))  # [[0, 0]]
