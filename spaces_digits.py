from string_checker import is_string


def is_only_string(text):
    return is_string(text) and ' ' in text and bool(len([n for n in text if n.isdigit()]))


if __name__ == '__main__':
    print(is_only_string('11'))  # False
    print(is_only_string(['hello']))  # ? Please handle this case!! Should return False
    print(is_only_string('this is a long sentence'))  # False
    print(is_only_string({'a': 2}))  # # ? Please handle this case!! Should return False
