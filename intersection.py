# Sort and Remove Duplicates
# Take 2 strings a and b including only letters from a to z. Return a new string that only contains each letter from a
# and/or b once and that is sorted. DO NOT USE set() ;)
print("Sort and Remove Duplicates")


def intersection(arg1, arg2):
    arg = arg1 + arg2
    res = ""
    for letter in arg:
        if not letter in res:
            res += letter
    return "".join(sorted(list(res)))


a = 'xyaabbbccccdefww'
b = 'xxxxyyyyabklmopq'
x = 'abcdefghijklmnopqrstuvwxyz'
print(intersection(a, b))  # abcdefklmopqwxy
print(intersection(a, x))  # abcdefghijklmnopqrstuvwxyz
