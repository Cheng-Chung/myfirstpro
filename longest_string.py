def total_string(a, b):
    c = a + b
    chars = []
    repeat = []
    check_repeat = []
    for char in c:
        if char not in chars:
            chars.append(char)
        else :
            repeat.append(char)
# the repeat character will be only show once
    for i in repeat:
        if i in repeat and i not in check_repeat:
            check_repeat.append(i)            

    no_repeat = ''.join(list(sorted(chars)))
    yes_repeat = ''.join(list(sorted(check_repeat)))
    return (no_repeat, yes_repeat)
    #return ''.join(list(sorted(repeat)))


a = 'adjjdaaajionS'
b = 'xxxxyyyyabklmopqA'
x = 'abcdefghijklmnopqrstuvwxyz'

print(total_string(a, b))  # ('ASabdijklmnopqxy', 'aaaadjjoxxxyyy') ('adjoxy')
print(total_string(a, x))  # ('Sabcdefghijklmnopqrstuvwxyz', 'aaaaddijjjno')('adijno')
